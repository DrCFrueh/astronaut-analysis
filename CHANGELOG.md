<!--
SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->

# Changelog

All notable changes will be documented in this file.

## [Unreleased]

## [1.0.0] - 2020-05-06

### Added

- Initial series of plots

[unreleased]: https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/compare/1.0.0...master
[1.0.0]: https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis/-/tags/1.0.0
