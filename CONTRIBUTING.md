<!--
SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)

SPDX-License-Identifier: MIT
-->

## General Options

- Please open **an issue**, if you have improvement ideas.
- Please open **a merge request**, if you want to share your improvements.

## Providing a Merge Request

- Please note that this **is not** a usual Git repository:
  - It demonstrates every important steps performed in the workshop.
  - Every branch shows the final results of a specific step.
  - The `default` branch adds further documentation and automates checking some details.
- If you provide a merge request:
  - Create the merge request against the branch which initially introduces the content that you want to change.
  - Then, we will take over and rebase all branches building on it.

**Example - Add change in `2-clean-up-code`:**

```
git switch 3<tab> && git rebase 2<tab> && git push -f
git switch 4<tab> && git rebase 3<tab> && git push -f
git switch 5<tab> && git rebase 4<tab> && git push -f
git switch 6<tab> && git rebase 5<tab> && git push -f
git switch check<tab> && git rebase 6<tab> && git push -f
```

> In case of merge conflicts, make sure to **skip** redundant changes.
